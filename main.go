package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	webPort := os.Getenv("WEB_PORT")
	db := SetupDb()

	dbStore := DbContext{Db: db}

	router := gin.Default()

	router.Use(RegisterDbContext(dbStore))

	ConfigureRouter(*router)

	router.Run(fmt.Sprintf("0.0.0.0:%s", webPort))
}
