package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func AliveHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "api is alive",
	})
}

func ConfigureRouter(r gin.Engine) {
	readings := r.Group("/readings")
	{
		readings.GET("/", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, gin.H{"a": "b"})
		})
		readings.PUT("/", CreateNewReading)
	}
	sensors := r.Group("/sensors")
	{
		sensors.GET("/", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, gin.H{"a": "b"})
		})
		sensors.PUT("/", CreateNewSensor)
	}

	r.GET("/", AliveHandler)
}
