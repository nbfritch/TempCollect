package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

type DbContext struct {
	Db *sql.DB
}

const (
	DbHostVar     = "DB_HOST"
	DbPortVar     = "DB_PORT"
	DbUserVar     = "DB_USER"
	DbPassVar     = "DB_PASS"
	DbDatabaseVar = "DB_DATABASE"
	DbUseSslVar   = "DB_USE_SSL"
)

func ReadDatabaseVariables() (string, error) {
	host := os.Getenv(DbHostVar)
	port := os.Getenv(DbPortVar)
	user := os.Getenv(DbUserVar)
	password := os.Getenv(DbPassVar)
	database := os.Getenv(DbDatabaseVar)
	useSsl := os.Getenv(DbUseSslVar)

	var sslMode = "disable"
	if useSsl == "TRUE" {
		sslMode = "enable"
	}

	if len(host) < 1 {
		return "", fmt.Errorf("ERR: Got host: '%s'", host)
	}

	if len(user) < 1 {
		return "", fmt.Errorf("ERR: Got user: '%s'", user)
	}

	if len(port) < 2 {
		return "", fmt.Errorf("ERR: Invalid port: %s", port)
	}

	if len(password) < 1 {
		return "", fmt.Errorf("ERR: Invalid password of len %d", len(password))
	}

	if len(database) < 1 {
		return "", fmt.Errorf("ERR: Invalid database '%d'", len(database))
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", host, user, password, database, port, sslMode)

	return dsn, nil
}

func SetupDb() *sql.DB {
	connStr, err := ReadDatabaseVariables()
	if err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	return db
}
