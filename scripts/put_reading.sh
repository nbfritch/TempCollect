#!/usr/bin/env sh

curl -X PUT 'http://localhost:4000/readings/' -H 'Content-Type: application/json' -d "{\"temperature\": 68.54, \"sensorName\": \"test\"}"