package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ReturnJsonError(c *gin.Context, err error, status int) {
	c.JSON(http.StatusInternalServerError, gin.H{
		"message": err,
	})
}
