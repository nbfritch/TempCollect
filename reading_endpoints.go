package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateReadingRequest struct {
	SensorName  string
	Temperature float64
}

func CreateNewReading(c *gin.Context) {
	var requestBody CreateReadingRequest
	if err := c.BindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Could not bind request",
		})
		return
	}

	db := GetDbContext(c).Db

	rows, err := db.Query("select id from sensors where name = $1", requestBody.SensorName)
	if err != nil {
		ReturnJsonError(c, err, http.StatusInternalServerError)
		return
	}

	var foundSensorId int64
	for rows.Next() {
		err = rows.Scan(&foundSensorId)
		if err != nil {
			ReturnJsonError(c, err, http.StatusInternalServerError)
			return
		}
	}

	rows, err = db.Query("insert into readings (sensor_id, temperature) values ($1, $2) returning id", foundSensorId, requestBody.Temperature)
	if err != nil {
		ReturnJsonError(c, err, http.StatusInternalServerError)
		return
	}

	var createdReadingId int64
	for rows.Next() {
		err = rows.Scan(&createdReadingId)
		if err != nil {
			ReturnJsonError(c, err, http.StatusInternalServerError)
			return
		}
	}

	c.JSON(http.StatusCreated, gin.H{
		"readingId": createdReadingId,
	})
}
