package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateSensorRequest struct {
	Name        string
	Description string
}

func CreateNewSensor(c *gin.Context) {
	var requestBody CreateSensorRequest
	if err := c.BindJSON(&requestBody); err != nil {
		ReturnJsonError(c, err, http.StatusBadRequest)
		return
	}

	db := GetDbContext(c).Db
	rows, err := db.Query("select id from sensors where name = $1", requestBody.Name)
	if err != nil {
		ReturnJsonError(c, err, http.StatusInternalServerError)
		return
	}

	existingSensorId := -1
	for rows.Next() {
		err = rows.Scan(&existingSensorId)
		if err != nil || existingSensorId != -1 {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": fmt.Sprintf("name taken by sensor with id %d", existingSensorId),
			})
			return
		}
	}

	rows, err = db.Query("insert into sensors (name, description) values($1, $2) returning id", requestBody.Name, requestBody.Description)
	if err != nil {
		ReturnJsonError(c, err, http.StatusInternalServerError)
		return
	}
	var sensorId int64
	for rows.Next() {
		err = rows.Scan(&sensorId)
		if err != nil {
			ReturnJsonError(c, err, http.StatusInternalServerError)
			return
		}
	}

	c.JSON(http.StatusCreated, gin.H{
		"sensorId": sensorId,
	})
}
